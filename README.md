## fs

file transport

### generate go message
1. install protobuf
2. `go install google.golang.org/protobuf/cmd/protoc-gen-go`

``` shell
$ cd entity
$ protoc -I=./ --go_out=./ message.proto
```